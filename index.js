import dictionary from 'dictionary-en'
import { promises as fs } from 'fs'
import retext from 'retext'
import spell from 'retext-spell'

const NAME = 'spell'

let _sp = null
async function initSpellChecker (wordsfile) {
  if (!_sp) {
    let words = ''
    if (wordsfile) {
      words = await fs.readFile(wordsfile)
    }
    _sp = retext().use(spell, {
      dictionary,
      personal: words,
      ignore: [
        'img',
        'src'
      ],
      ignoreDigits: true,
      ignoreLiteral: true
    })
  }
  return _sp
}

async function spProcess (text) {
  return new Promise((resolve, reject) => {
    _sp.process(text, (err, file) => {
      if (err) {
        reject(err)
        return
      }
      resolve(file)
    })
  })
}

async function spellCheck (text, ignore = [], opts) {
  text = text
    .replace(/\n/g, ' ')
    .replace(/&\w*;/g, '')
    .replace(/\u00AD/g, '')
    .replace(/\bhttps?:\/\/.*( |$)/g, ' ')
    .trim()
  if (!text.match(/\w/)) return []
  const file = await spProcess(text)
  const messages = []

  for (const m of file.messages) {
    // If there is no m.actual, then it's just a warning about capacity.
    if (!m.actual) continue
    if (m.actual.match(/[0-9@.:]/)) continue
    if (ignore.includes(m.actual)) continue
    const subwords = m.actual.split('-')
    if (subwords.length > 1) {
      const newMessages = await spellCheck(subwords.join(' '), ignore)
      messages.push(...newMessages)
      continue
    }

    let expected = ''
    if (m.expected) expected = ` (${m.expected})`
    messages.push(`${m.message}${expected}`)
  }
  return messages
}

// TODO: Move this out to its own package.
export default (opts) => {
  opts = Object.assign({
    ignoretypes: []
  }, opts)
  return async (root, f) => {
    await initSpellChecker(opts.wordsfile)

    let enabled = true
    const handleNode = async n => {
      if (opts.ignoretypes.includes(n.type)) return
      if (n.attributes?.nospell) return
      const type = (typeof n.value)
      switch (n.type) {
        case 'text-nospell':
          enabled = false
          n.type = 'span'
          break
        case 'text-yesspell':
          enabled = true
          n.type = 'span'
          break
        case 'text':
          if (!enabled) break
          if (type !== 'string') {
            f.message(`Type "text" node has non-string type "${type}"`,
              n.position, `${NAME}:non-string`)
          }
          for (const m of await spellCheck(n.value ?? '')) {
            // TODO: Get a more specific position.
            f.message(m, n.position, `${NAME}:correct`)
          }
          break
        default:
          for (const c of n.children || []) {
            await handleNode(c)
          }
      }
    }
    await handleNode(root)
  }
}
